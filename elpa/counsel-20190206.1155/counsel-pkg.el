;;; -*- no-byte-compile: t -*-
(define-package "counsel" "20190206.1155" "Various completion functions using Ivy" '((emacs "24.3") (swiper "0.11.0")) :commit "b01108e167ad26139535a516a056063d7ea4fff6" :keywords '("convenience" "matching" "tools") :authors '(("Oleh Krehel" . "ohwoeowho@gmail.com")) :maintainer '("Oleh Krehel" . "ohwoeowho@gmail.com") :url "https://github.com/abo-abo/swiper")
