;;; -*- no-byte-compile: t -*-
(define-package "ace-window" "20190210.1648" "Quickly switch windows." '((avy "0.2.0")) :commit "d7bdb5362275a8f82a45cad6eb3583f3f48f823a" :keywords '("window" "location") :authors '(("Oleh Krehel" . "ohwoeowho@gmail.com")) :maintainer '("Oleh Krehel" . "ohwoeowho@gmail.com") :url "https://github.com/abo-abo/ace-window")
